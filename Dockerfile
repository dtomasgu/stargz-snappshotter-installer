FROM fedora:latest

ARG VERSION="v0.14.3"

RUN mkdir stargz-snapshotter
RUN curl -L https://github.com/containerd/stargz-snapshotter/releases/download/$VERSION/stargz-snapshotter-$VERSION-linux-amd64.tar.gz -o stargz-snapshotter/stargz-snapshotter.tar.gz; \
    curl -L https://raw.githubusercontent.com/containerd/stargz-snapshotter/main/script/config/etc/systemd/system/stargz-snapshotter.service -o stargz-snapshotter/stargz-snapshotter.service

